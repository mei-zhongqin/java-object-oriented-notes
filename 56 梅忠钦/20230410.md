```java
# 作业

```java
import java.util.Arrays;

public class H1 {
    //    要求：
//    1.定义工具类，可以用来求和，求最大值，最小值，给数组从小到大排序，给数组大小到排序。从数组中找一个元素的位置，
//    2.再定义一个测试类，测试上面的方法
    public static int indexOf(int[] arr, int value) {
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] == value) {
                return i;
            }
        }
        return -1;
    }
    public static int getSum(int[] arr) {
        int sum = arr[0];
        for (int i = 0; i < arr.length; i++) {
            sum += arr[i];
        }
        return sum;
    }

    public static int getMax(int[] arr) {
        int max = arr[0];
        for (int i = 0; i < arr.length; i++) {
            if (max < arr[i]) {
                max = arr[i];
            }
        }
        return max;
    }

    public static int getMin(int[] arr) {
        int min = arr[0];
        for (int i = 0; i < arr.length; i++) {
            if (min > arr[i]) {
                min = arr[i];
            }
        }
        return min;
    }

    public static int[] sort(int[] arr) {
        for (int i = 0; i < arr.length; i++) {
            int temp;
            for (int j = 0; j < arr.length; j++) {
                if (arr[i] > arr[j]) {
                    temp = arr[i];
                    arr[i]=arr[j];
                    arr[j] = temp;
            }
            }
        }
        return arr;
    }
    public static int[] sort1(int[] arr) {
        int temp;
        for (int i=0; i < arr.length; i++) {
            for (int j = 0; j < arr.length; j++) {
                if (arr[i] < arr[j]) {
                    temp = arr[i];
                    arr[i] = arr[j];
                    arr[j] = temp;

                }
            }
        }return arr;
    }

}


import java.util.Arrays;

public class H2 {
    public static void main(String[] args) {
        int[] arr={1,54,63,25,48,74};
        System.out.println("元素63位置："+H1.indexOf(arr,63));
        System.out.println("最大值为"+H1.getMax(arr));
        System.out.println("最小值为"+H1.getMin(arr));
        System.out.println("和为"+H1.getSum(arr));
        int[] sort =H1.sort(arr);
        System.out.println("从大到小："+Arrays.toString(sort));
        int[] sort1 =H1.sort1(arr);
        System.out.println("从小到大："+Arrays.toString(sort1));
    }
}
```


